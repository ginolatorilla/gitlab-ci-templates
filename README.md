# Gino's GitLab CI Templates

## Docker

This template will add the following jobs to your CI/CD pipeline:

- **docker build** builds the Docker image. This job will run in the **build** stage.

- **docker push** builds the Docker image and then pushes it with the **latest** tag. This job will run in
  the **deploy** stage and will only appear after a merge request is accepted.

- **docker push tags** builds the Docker image and then pushes it with the current Git tag in addition to the **latest** tag.
  This job will run in the **deploy** stage and will only appear if a tag is created.

### Usage

Copy `docker.gitlab-ci.yml` and add the following lines in your project's `.gitlab-ci.yml`:

```yaml
include:
  - <path-to>/docker.gitlab-ci.yml

variables:
  # See the following table.
```

| CICD Variable           | Description                                                                                                                     |
| ----------------------- | ------------------------------------------------------------------------------------------------------------------------------- |
| `DOCKER_DIND_IMAGE`     | The Docker-in-Docker image all the CI jobs will use. Defaults to `docker:dind`                                                  |
| `DOCKER_BUILD_DIR`      | Run `docker build` in this directory; this is where the `Dockerfile` should be found. Defaults to the project's root directory. |
| `DOCKER_BUILD_ARGS`     | Additional CLI arguments that will be passed when calling the `docker build` command.                                           |
| `DOCKER_REGISTRY_IMAGE` | The name of the image, including the registry and subpaths. Defaults to the project's container registry image name.            |
| `DOCKER_REGISTRY`       | The URL of a Docker registry to authenticate with. Defaults to the project's container registry.                                |
| `DOCKER_LOGIN_USERNAME` | Log-in to a Docker registry as this user. Defaults to a GitLab-generated one with job-scope.                                    |
| `DOCKER_LOGIN_PASSWORD` | Password for authenticating to a Docker registry. Defaults to a GitLab-generated one with job-scope.                            |
| `DOCKER_AUTH_CONFIG`    | Use this instead of `DOCKER_LOGIN_USERNAME` and `DOCKER_LOGIN_PASSWORD`. Must contain a Docker config JSON document.            |

### Example

<https://gitlab.com/ginolatorilla/gitlab-ci-templates-docker-example>

## Python

This template will add the following jobs to your CI/CD pipeline:

- **venv** prepares the Python Virtual environment that will be used by all jobs in this template.
  This environment will be cached. This job will run in the **prepare** stage. This requires that
  your project is a Python package (`setup.py` or `pyproject.toml`) and can be called with
  `pip install <project-dir>`.

- **pytest** will run PyTest at the project's base path. This job will run in the **test** stage.
  This requires that your project has a dependency to `pytest`.

- **flake8** will run Flake8 at the project's base path. This job will run in the **test** stage.
  This requires that your project has a dependency to `flake8`.

- **black** will run Black *with formatting checks only* at the project's base path.
  This job will run in the **test** stage. This requires that your project has a dependency to `black`.

- **generate git tag** will predict the future Git tag when a merge request is accepted.
  This job will run in the **test** stage. This requires that your project has a dependency to `setuptools_scm`.

- **promote release** will push to GitLab the tag created by the **generate git tag** job.
  This job will run in the **tag** stage and will only appear after a merge request is accepted.
  This is a **manually-triggered** job so you have the option not to.

- **publish package** will build your project into wheels and push them to a PyPi registry.
  This job will run in the **publish** stage and will only appear after a merge request is accepted.
  This requires that your project has a dependency to `twine`.

This template will also set some workflow rules for pipeline creation.

- Allow merge request pipelines.
- Allow pipelines for the default branch (ie `main`, `master`, or whichever your project uses).
- For all other possible triggers, do not create pipelines. For example, pushing new branches without merge request
  will not create pipelines.

### Usage

Copy `python.gitlab-ci.yml` and add the following lines in your project's `.gitlab-ci.yml`:

```yaml
include:
  - <path-to>/python.gitlab-ci.yml

variables:
  # See the following table.
```

| CICD Variable                     | Description                                                                                                                             |
| --------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------- |
| `PYTHON_DOCKER_IMAGE`             | The Python Docker image all the CI jobs will use. Defaults to `python:3.11`                                                             |
| `PYTHON_VENV_DIR`                 | The name of the Python Virtual Environment directory                                                                                    |
| `PYTHON_PROJECT_REQUIREMENT_SPEC` | Defaults to `[dev]`. If your project defines requirement groups, the **venv** job will call it as: `pip install .[dev]`.                |
| `PYTHON_ENABLED_TESTS`            | Enable test jobs. This should be a list of strings from this list: pytest, flake8, black. For example, "pytest flake8". Defaults to "". |
| `PYTHON_DYNAMIC_SEMVER`           | If set, enables these jobs: generate git tag, promote release, and publish package. Defaults to "true".                                 |
| `PYTHON_GIT_TAG_PREFIX`           | Adds a prefix in front of the Git tags. Defaults to "v".                                                                                |
| `TWINE_REPOSITORY_URL`            | Set this to use a different PyPI URL. Defaults to the current project's package registry.                                               |
| `TWINE_USERNAME`                  | Log-in to a PyPI as this user. Defaults to `gitlab-ci-token`.                                                                           |
| `TWINE_PASSWORD`                  | Password for authenticating to a PyPI. Defaults to the CI job's token.                                                                  |

### Example

<https://gitlab.com/ginolatorilla/gitlab-ci-templates-python-example>

## Terraform

I based this template on GitLab's official [Terraform CI template](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Terraform/Base.latest.gitlab-ci.yml).

### Usage

There are three variants of this template:

- `terraform.gitlab-ci.yml` is for simple Terraform projects with one state. The project root directory contains the Terraform files.
- `terraform-multistage.gitlab-ci.yml` is for Terraform projects with a `stg` and `prod` state. The Terraform files
  should be in `environments/stg` and `environments/prod`, respectively.
- `terraform-base.gitlab-ci.yml` is for all other use cases. Both previous templates use this template.

Copy `terraform-base.gitlab-ci.yml`, `terraform.gitlab-ci.yml`, `terraform-multistage.gitlab-ci.yml` and add the following lines in your project's `.gitlab-ci.yml`:

```yaml
include:
  - <path-to>/terraform.gitlab-ci.yml # Use only one of the variants.

variables:
  # See the following table.
```

| CICD Variable            | Description                                                                                                                |
| ------------------------ | -------------------------------------------------------------------------------------------------------------------------- |
| `TERRAFORM_IMAGE`        | The GitLab Terraform runner image to use.                                                                                  |
| `TERRAFORM_CHECK_FORMAT` | Set to `"true"` to run `terraform validate`, otherwise it skips this job.                                                  |
| `TF_STATE_NAME`          | Change the state name when stored in GitLab. This variable only applies to `terraform.gitlab-ci.yml`.                      |
| `TF_ROOT`                | Points to a relative directory where to find the Terraform files. This variable only applies to `terraform.gitlab-ci.yml`. |

⚠️ Attention: After HashiCorp decides to change its licences for Terraform, GitLab will no longer support and maintain its Terraform runner image.
Follow the instructions [here](https://gitlab.com/gitlab-org/terraform-images#build-and-host-the-image-and-template-yourself-for-up-to-date-terraform)
to build the Terraform runner image, and set `TERRAFORM_IMAGE`` to this image.
